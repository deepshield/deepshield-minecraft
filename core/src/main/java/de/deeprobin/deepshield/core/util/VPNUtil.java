package de.deeprobin.deepshield.core.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class VPNUtil {

    public static boolean isVPN(String ip){
        try {
        final URL url = new URL("http://ip-api.com/json/" + ip);
        BufferedReader in =  new BufferedReader(
                    new InputStreamReader(url.openStream()));

        Gson gson = new Gson();
        JsonObject obj = gson.fromJson(in, JsonObject.class);
        return obj.get("proxy").getAsBoolean();
        } catch (IOException e) {
            return false;
        }
    }

}
