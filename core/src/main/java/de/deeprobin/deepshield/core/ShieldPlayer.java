package de.deeprobin.deepshield.core;

import java.util.UUID;

public interface ShieldPlayer {

    UUID getUniqueId();
    String getName();

}
