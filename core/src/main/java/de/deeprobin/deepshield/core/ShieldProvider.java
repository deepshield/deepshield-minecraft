package de.deeprobin.deepshield.core;

public interface ShieldProvider {
    void reportToStaff(String message);
}
