package de.deeprobin.deepshield.core;

public enum Significance {


    NEGLIGIBLE(0),
    VERY_LOW(1),
    LOW(2),
    MEDIUM(3),
    HIGH(4),
    CRITICAL(5);

    private final int value;

    Significance(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
