# DeepShield - Minecraft

This is the minecraft adapter of the DeepShield security service. This universal plugin fixes many exploits 
in minecraft servers and plugins. And it protects the server from vulnerabilities.