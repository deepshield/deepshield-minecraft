-dontwarn java.**
-dontwarn android.**
-dontwarn org.bukkit.**
-dontwarn com.comphenix.protocol.**
# -dontwarn **
-dontshrink
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}