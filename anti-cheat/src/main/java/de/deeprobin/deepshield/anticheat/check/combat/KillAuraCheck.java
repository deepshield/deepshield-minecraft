package de.deeprobin.deepshield.anticheat.check.combat;

import de.deeprobin.deepshield.anticheat.*;
import de.deeprobin.deepshield.anticheat.check.Check;
import de.deeprobin.deepshield.anticheat.event.MoveEvent;
import de.deeprobin.deepshield.anticheat.event.PlayerHitOtherPlayerEvent;
import de.deeprobin.deepshield.anticheat.util.MathUtil;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class KillAuraCheck extends Check {

    private static Map<UUID, Location> lastloc = new HashMap<UUID, Location>();

    public KillAuraCheck(final AntiCheat antiCheat) {
        super("Kill Aura", antiCheat);
    }

    @Subscribe
    public void handleMove(final MoveEvent event){
        lastloc.put(event.getPlayer().getUniqueId(), event.getTo());
    }

    @Subscribe
    public void handleHit(final PlayerHitOtherPlayerEvent event){
        ACPlayer player = event.getPlayer();
        ACPlayer target = event.getTarget();

        if(player.isBlocking()){
            return;
        }

        if(player.isSleeping()){
            return;
        }

        if(player.isDead()) {
            return;
        }

        if(event.getCause() == DamageCause.ENTITY_SWEEP_ATTACK){
            return;
        }

        boolean lineOfSight = false;
        Vector line = player.getLocation().toVector().clone().subtract(target.getLocation().toVector())
                .normalize();
        Vector dirFacing = target.getEyeLocation().getDirection().clone().normalize();
        double angle = Math.acos(line.dot(dirFacing));
        if (angle > 0.785398163) {
            lineOfSight = true;
        }
        if(!lastloc.containsKey(player.getUniqueId()) || player.getGameMode() == GameMode.CREATIVE || player.getGameMode() == GameMode.SPECTATOR){
            return; // move event might not update (probably in creative)
        }
        double oy = Math.abs(lastloc.get(player.getUniqueId()).getYaw());
        double op = Math.abs(lastloc.get(player.getUniqueId()).getPitch());
        double cy = Math.abs(player.getLocation().getYaw());
        double cp = Math.abs(player.getLocation().getPitch());
        boolean f_yaw = Math.abs(MathUtil.trim(1, cy - oy)) > 35;
        boolean f_pitch = Math.abs(MathUtil.trim(1, cp - op)) > 35;
        if (f_yaw || f_pitch) {
            this.detect(event.getPlayer(), "Irregular head snapping");
            event.cancel();
            return;
        }
        if (lineOfSight) {
            this.detect(event.getPlayer(), "Hit target not in sight");
            event.cancel();
        }
    }

}
