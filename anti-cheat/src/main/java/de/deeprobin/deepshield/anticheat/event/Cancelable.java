package de.deeprobin.deepshield.anticheat.event;

public interface Cancelable {

    default void cancel(){
        this.setCancelled(true);
    }

    void setCancelled(boolean bool);

    boolean isCancelled();

}
