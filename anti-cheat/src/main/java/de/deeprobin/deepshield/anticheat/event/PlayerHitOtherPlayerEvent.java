package de.deeprobin.deepshield.anticheat.event;

import de.deeprobin.deepshield.anticheat.ACPlayer;
import de.deeprobin.deepshield.anticheat.DamageCause;

public class PlayerHitOtherPlayerEvent extends Event implements Cancelable {

    private final ACPlayer player;
    private final ACPlayer target;
    private final DamageCause cause;

    private boolean cancelled = false;

    public PlayerHitOtherPlayerEvent(ACPlayer player, ACPlayer target, DamageCause cause) {
        this.player = player;
        this.target = target;
        this.cause = cause;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    public ACPlayer getPlayer() {
        return player;
    }

    public ACPlayer getTarget() {
        return target;
    }

    public DamageCause getCause() {
        return cause;
    }
}
