package de.deeprobin.deepshield.anticheat;

import de.deeprobin.deepshield.core.ShieldPlayer;

public interface ACPlayer extends ShieldPlayer {

    Location getLocation();
    Location getEyeLocation();
    boolean isBlocking();
    boolean isSleeping();
    boolean isDead();

    GameMode getGameMode();
}
