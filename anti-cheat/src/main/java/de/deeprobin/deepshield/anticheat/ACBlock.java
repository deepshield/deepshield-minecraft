package de.deeprobin.deepshield.anticheat;

public interface ACBlock {

    public int getX();
    public int getY();
    public int getZ();

    public String getId();

}
