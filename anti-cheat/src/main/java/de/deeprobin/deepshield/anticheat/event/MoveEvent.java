package de.deeprobin.deepshield.anticheat.event;

import de.deeprobin.deepshield.anticheat.ACPlayer;
import de.deeprobin.deepshield.anticheat.Location;

public class MoveEvent extends Event implements Cancelable {

    private final Location from;
    private final Location to;

    private final ACPlayer player;


    public MoveEvent(Location from, Location to, ACPlayer player) {
        this.from = from;
        this.to = to;
        this.player = player;
    }

    public Location getFrom() {
        return from;
    }

    public Location getTo() {
        return to;
    }

    public ACPlayer getPlayer() {
        return player;
    }

    private boolean cancelled;

    @Override
    public void setCancelled(boolean bool) {
        this.cancelled = bool;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }
}
