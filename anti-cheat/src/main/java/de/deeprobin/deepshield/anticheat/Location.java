package de.deeprobin.deepshield.anticheat;

import de.deeprobin.deepshield.anticheat.util.MathUtil;
import org.jetbrains.annotations.NotNull;

public class Location {

    private final double x;
    private final double y;
    private final double z;
    private float pitch;
    private float yaw;

    public Location(double x, double y, double z, float pitch, float yaw) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public Location(float x, float y, float z) {
        this(x, y, z, 0, 0);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public Vector getDirection(){
        Vector vector = new Vector();

        double rotX = this.getYaw();
        double rotY = this.getPitch();

        vector.setY(-Math.sin(Math.toRadians(rotY)));

        double xz = Math.cos(Math.toRadians(rotY));

        vector.setX(-xz * Math.sin(Math.toRadians(rotX)));
        vector.setZ(xz * Math.cos(Math.toRadians(rotX)));

        return vector;
    }

    @NotNull
    public Vector toVector() {
        return new Vector(x, y, z);
    }

    public double distanceSquared(@NotNull Location o) {
        return MathUtil.square(x - o.x) + MathUtil.square(y - o.y) + MathUtil.square(z - o.z);
    }

    public double distance(@NotNull Location o) {
        return Math.sqrt(distanceSquared(o));
    }
}
