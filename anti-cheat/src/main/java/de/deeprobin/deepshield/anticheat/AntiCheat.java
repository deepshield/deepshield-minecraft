package de.deeprobin.deepshield.anticheat;

import de.deeprobin.deepshield.anticheat.check.combat.KillAuraCheck;
import de.deeprobin.deepshield.anticheat.check.combat.ReachCheck;
import de.deeprobin.deepshield.core.ShieldProvider;
import de.deeprobin.deepshield.anticheat.check.combat.MultiAuraCheck;
import de.deeprobin.deepshield.anticheat.manager.CheckManager;

public final class AntiCheat {

    private final ShieldProvider shieldProvider;
    private final CheckManager checkManager;

    public AntiCheat(ShieldProvider shieldProvider) {
        this.shieldProvider = shieldProvider;
        this.checkManager = new CheckManager(this);
        this.checkManager.registerCheck(new KillAuraCheck(this));
        this.checkManager.registerCheck(new MultiAuraCheck(this));
        this.checkManager.registerCheck(new ReachCheck(this));
    }

    public ShieldProvider getShieldProvider() {
        return shieldProvider;
    }

    public CheckManager getCheckManager() {
        return checkManager;
    }
}
