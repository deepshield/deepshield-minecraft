package de.deeprobin.deepshield.anticheat.manager;

import de.deeprobin.deepshield.anticheat.AntiCheat;
import de.deeprobin.deepshield.anticheat.check.Check;
import org.greenrobot.eventbus.EventBus;

import java.util.LinkedList;
import java.util.List;

public final class CheckManager {

    private final List<Check> checks;
    private final EventBus eventBus;

    public CheckManager(AntiCheat antiCheat) {
        this.checks = new LinkedList<>();
        this.eventBus = new EventBus();
    }


    public void registerCheck(Check check){
        this.checks.add(check);
        this.eventBus.register(check);
    }

    public List<Check> getChecks() {
        return checks;
    }

    public EventBus getEventBus() {
        return eventBus;
    }
}
