package de.deeprobin.deepshield.anticheat.check.combat;

import de.deeprobin.deepshield.anticheat.AntiCheat;
import de.deeprobin.deepshield.anticheat.check.Check;
import de.deeprobin.deepshield.anticheat.event.PlayerHitOtherPlayerEvent;
import org.greenrobot.eventbus.Subscribe;

public class ReachCheck extends Check {

    public ReachCheck(final AntiCheat antiCheat) {
        super("Reach", antiCheat);
    }

    @Subscribe
    public void handleHit(PlayerHitOtherPlayerEvent event){
        double range = event.getTarget().getLocation().distance(event.getPlayer().getLocation());
        if (range > 5.98) {
            detect(event.getPlayer(), String.format("Range: %1$,.2f blocks", range));
            event.cancel();
        }
    }

}
