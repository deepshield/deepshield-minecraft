package de.deeprobin.deepshield.anticheat.check;

import de.deeprobin.deepshield.anticheat.ACPlayer;
import de.deeprobin.deepshield.anticheat.AntiCheat;

public class Check {

    private final String name;
    private final AntiCheat antiCheat;

    public Check(String name, AntiCheat antiCheat) {
        this.name = name;
        this.antiCheat = antiCheat;
    }

    public final String getName() {
        return name;
    }

    protected final AntiCheat getAntiCheat() {
        return antiCheat;
    }

    protected final void detect(ACPlayer player, String message){
        this.getAntiCheat().getShieldProvider().reportToStaff(String.format("AC: %s uses %s(%s)", player.getName(), this.getName(), message));
    }
}
