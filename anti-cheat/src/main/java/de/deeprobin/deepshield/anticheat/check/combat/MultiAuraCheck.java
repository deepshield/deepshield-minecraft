package de.deeprobin.deepshield.anticheat.check.combat;

import de.deeprobin.deepshield.anticheat.ACPlayer;
import de.deeprobin.deepshield.anticheat.AntiCheat;
import de.deeprobin.deepshield.anticheat.event.PlayerHitOtherPlayerEvent;
import de.deeprobin.deepshield.anticheat.DamageCause;
import de.deeprobin.deepshield.anticheat.Location;
import de.deeprobin.deepshield.anticheat.check.Check;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MultiAuraCheck extends Check {


    private static Map<UUID, Map<Long, Location>> lastHit = new HashMap<UUID, Map<Long, Location>>();

    public MultiAuraCheck(final AntiCheat antiCheat) {
        super("Multi Aura", antiCheat);
    }

    @Subscribe
    public void handleHit(final PlayerHitOtherPlayerEvent event){
        ACPlayer player = event.getPlayer();
        Location hit = event.getTarget().getLocation();
        boolean ret = false;
        if(event.getCause() == DamageCause.ENTITY_SWEEP_ATTACK){
            return;
        }

        if(lastHit.containsKey(player.getUniqueId())){
            long time = System.currentTimeMillis() - lastHit.get(player.getUniqueId()).keySet().iterator().next();
            Location last = lastHit.get(player.getUniqueId()).values().iterator().next();
            // TODO: add same world check
            double distance = last.distance(hit);

            if (distance > 1.5 && time < 8) {
                ret = true;
            }

            Map<Long, Location> r = new HashMap<Long, Location>();
            r.put(System.currentTimeMillis(), hit);
            lastHit.put(player.getUniqueId(), r);
            if (ret) {
                this.detect(player, "Hits multiple players in less than 8ms");
                event.cancel();
            }

        }


    }

}
