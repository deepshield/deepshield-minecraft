package de.deeprobin.deepshield.anticheat;

public enum GameMode {
    CREATIVE,
    SPECTATOR,
    SURVIVAL,
    ADVENTURE
}
