package de.deeprobin.deepshield.bukkit;

import de.deeprobin.deepshield.core.Significance;

public class PluginResult {

    private final boolean vulnerable;

    private Significance significance;

    private String additionalInformation = "No additional information";

    public PluginResult(boolean vulnerable, Significance significance, String additionalInformation) {
        this(vulnerable, significance);
        this.additionalInformation = additionalInformation;
    }

    public PluginResult(boolean vulnerable, Significance significance) {
        this(vulnerable);
        this.significance = significance;
    }

    public PluginResult(Significance significance) {
        this(true, significance);
    }

    public PluginResult(Significance significance, String additionalInformation) {
        this(significance);
        this.additionalInformation = additionalInformation;
    }

    public PluginResult(boolean vulnerable){
        this.vulnerable = vulnerable;
    }

    public boolean isVulnerable() {
        return vulnerable;
    }

    public Significance getSignificance() {
        return significance;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }
}
