package de.deeprobin.deepshield.bukkit;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.deeprobin.deepshield.core.ShieldProvider;
import de.deeprobin.deepshield.anticheat.AntiCheat;
import de.deeprobin.deepshield.bukkit.adapters.ServerPingListener;
import de.deeprobin.deepshield.bukkit.component.item.SkullExploitComponent;
import de.deeprobin.deepshield.bukkit.component.item.book.BookExploitComponent;
import de.deeprobin.deepshield.bukkit.component.item.book.BookPageSizeComponent;
import de.deeprobin.deepshield.bukkit.component.mod.SignComponent;
import de.deeprobin.deepshield.bukkit.component.packet.CustomPayloadComponent;
import de.deeprobin.deepshield.bukkit.component.packet.PositionComponent;
import de.deeprobin.deepshield.bukkit.component.plugin.CashPloitPluginComponent;
import de.deeprobin.deepshield.bukkit.component.plugin.DirectLeaksPluginComponent;
import de.deeprobin.deepshield.bukkit.component.plugin.ForceOpPluginComponent;
import de.deeprobin.deepshield.bukkit.listener.*;
import de.deeprobin.deepshield.bukkit.manager.ComponentManager;
import de.deeprobin.deepshield.bukkit.worker.PluginChecker;
import de.deeprobin.deepshield.bukkit.worker.UpdateChecker;
import lombok.Getter;
import org.bstats.bukkit.Metrics;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class DeepShieldPlugin extends JavaPlugin implements ShieldProvider {

    @Getter
    private ComponentManager componentManager;

    @Getter
    private ProtocolManager protocolManager;

    private AntiCheat antiCheat;

    @Override
    public void onEnable(){
        long benchmarkStartTime = System.currentTimeMillis();
        this.protocolManager = ProtocolLibrary.getProtocolManager();
        this.antiCheat = new AntiCheat(this);
        this.componentManager = new ComponentManager(this);
        this.componentManager.registerComponent(new SkullExploitComponent(this));
        this.componentManager.registerComponent(new CustomPayloadComponent(this));
        this.componentManager.registerComponent(new BookExploitComponent(this));
        this.componentManager.registerComponent(new BookPageSizeComponent(this));
        this.componentManager.registerComponent(new SignComponent(this));
        this.componentManager.registerComponent(new PositionComponent(this));
        this.componentManager.registerComponent(new ForceOpPluginComponent(this));
        this.componentManager.registerComponent(new CashPloitPluginComponent(this));
        this.componentManager.registerComponent(new DirectLeaksPluginComponent(this));

        this.protocolManager.addPacketListener(new ServerPingListener(this));
        this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        this.getServer().getPluginManager().registerEvents(new ItemListener(this), this);
        this.getServer().getPluginManager().registerEvents(new WorldListener(this), this);
        this.getServer().getPluginManager().registerEvents(new AntiCheatListener(this), this);
        this.getServer().getPluginManager().registerEvents(new VPNListener(), this);

        Metrics metrics = new Metrics(this);
        if(!metrics.isEnabled()) {
            this.getLogger().warning("Please enable bStats to send anonymous collected data to the developers of the plugin.");
        }
        this.getLogger().info(String.format("Done (Took %dms).", (System.currentTimeMillis() - benchmarkStartTime)));

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new PluginChecker(this), 0, 300 * 20);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new UpdateChecker(this), 0, 1200 * 20);
    }

    @Override
    public void reportToStaff(String message){
        for(Player player : this.getServer().getOnlinePlayers()){
            if(player.hasPermission("deepshield.notify")){
                player.sendMessage(String.format("§9§l[DeepShield] §r§9%s", message));
            }
        }
    }

    public AntiCheat getAntiCheat() {
        return antiCheat;
    }
}
