package de.deeprobin.deepshield.bukkit.component.item;

import de.deeprobin.deepshield.bukkit.Result;
import org.bukkit.inventory.ItemStack;

public interface ItemComponent {

    public Result isExploit(ItemStack itemStack);

}
