package de.deeprobin.deepshield.bukkit.worker;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.util.SpigotUpdateChecker;
import org.bukkit.plugin.Plugin;

import java.net.URI;
import java.net.URISyntaxException;

public class UpdateChecker implements Runnable {


    private DeepShieldPlugin plugin;

    public UpdateChecker(DeepShieldPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        this.plugin.getLogger().info("Checking updates: STARTED");
        for (Plugin cPlugin : plugin.getServer().getPluginManager().getPlugins()) {
            try {
                if (getDomainName(cPlugin.getDescription().getWebsite()).equals("spigotmc.org")) {
                    int id = getResourceId(cPlugin.getDescription().getWebsite());
                    new SpigotUpdateChecker(cPlugin, id).getVersion(version -> {
                                if (!version.equals(cPlugin.getDescription().getVersion())) {
                                    plugin.reportToStaff(String.format("Plugin %s (%s) is out of date. It is recommended to download version %s", cPlugin.getName(), cPlugin.getDescription().getVersion(), version));
                                }
                            }
                    );
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        this.plugin.getLogger().info("Checking updates: DONE");
    }

    private static String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost().toLowerCase();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    private static int getResourceId(String url) throws URISyntaxException, ArrayIndexOutOfBoundsException {
        URI uri = new URI(url);
        String[] bananaSplit = uri.getPath().split(".");
        return Integer.parseUnsignedInt(bananaSplit[bananaSplit.length - 1]);
    }
}
