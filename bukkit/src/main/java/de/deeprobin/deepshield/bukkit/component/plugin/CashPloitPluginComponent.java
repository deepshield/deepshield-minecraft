package de.deeprobin.deepshield.bukkit.component.plugin;

import com.google.common.io.ByteStreams;
import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.PluginResult;
import de.deeprobin.deepshield.core.Significance;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@ComponentInfo(name = "plugin-cashploit")
public class CashPloitPluginComponent extends Component implements PluginComponent {

    private static final Base64.Decoder DECODER = Base64.getDecoder();

    public CashPloitPluginComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @Override
    public PluginResult checkPlugin(File file) {
        try {
            if(isCashploitData(getInfoClass(new ZipFile(file)))){
                return new PluginResult(Significance.CRITICAL, "CashPloit");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new PluginResult(false);
    }

    private byte[] getInfoClass(ZipFile zip) {
        try {
            String filePath = zip.getName();
            Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements()) {
                ZipEntry e = entries.nextElement();
                if (e.getName().equals("info.class")) {
                    //setClassNodePath(e.getName());
                    byte[] content;
                    try (ZipFile tempZip = new ZipFile(filePath)) {
                        content = ByteStreams.toByteArray(tempZip.getInputStream(e));
                    }
                    return content;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isCashploitData(byte[] content) {
        if (content == null) {
            return false;
        }
        try {
            //Cashploit is a griefing plugin which stores its trustcommand in the info.class (3x base64 encoded)
            DECODER.decode(DECODER.decode(DECODER.decode(content)));
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
