package de.deeprobin.deepshield.bukkit.component;


import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;

import java.util.logging.Logger;

public abstract class Component {

    protected DeepShieldPlugin plugin;
    protected Logger logger;

    public Component(DeepShieldPlugin plugin){
        this.plugin = plugin;
        this.logger = plugin.getLogger();
    }

}
