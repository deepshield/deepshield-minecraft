package de.deeprobin.deepshield.bukkit.listener;

import de.deeprobin.deepshield.anticheat.DamageCause;
import de.deeprobin.deepshield.anticheat.event.MoveEvent;
import de.deeprobin.deepshield.anticheat.event.PlayerHitOtherPlayerEvent;
import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.impl.BukkitPlayer;
import de.deeprobin.deepshield.bukkit.impl.ConvUtil;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.greenrobot.eventbus.EventBus;

public final class AntiCheatListener implements Listener {

    private final DeepShieldPlugin plugin;
    private EventBus bus;

    public AntiCheatListener(DeepShieldPlugin plugin) {
        this.plugin = plugin;
        this.bus = this.plugin.getAntiCheat().getCheckManager().getEventBus();
    }

    @EventHandler
    public void handleMove(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();
        if (to != null) {
            MoveEvent newEvent = new MoveEvent(ConvUtil.toAC(from), ConvUtil.toAC(to), new BukkitPlayer(event.getPlayer()));
            this.bus.post(newEvent);
            event.setCancelled(newEvent.isCancelled());
        }
    }

    @EventHandler
    public void handleMove(EntityDamageByEntityEvent event) {
        Entity entity = event.getDamager();
        Entity target = event.getEntity();
        if (entity instanceof Player && target instanceof Player) {
            DamageCause cause = DamageCause.values()[event.getCause().ordinal()];
            PlayerHitOtherPlayerEvent newEvent = new PlayerHitOtherPlayerEvent(new BukkitPlayer((Player) entity), new BukkitPlayer((Player) target), cause);
            this.bus.post(newEvent);
            event.setCancelled(newEvent.isCancelled());
        }
    }
}
