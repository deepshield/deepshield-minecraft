package de.deeprobin.deepshield.bukkit.component.mod;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.component.Component;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.Listener;

public abstract class ClientModComponent extends Component implements Listener {
    public ClientModComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }


    protected void detect(OfflinePlayer player){
        this.detect(player, "No additional information");
    }

    protected void detect(OfflinePlayer player, CharSequence reason){
        this.plugin.reportToStaff(String.format("%s uses client modifications [%s]", player.getName(), reason));
    }
}
