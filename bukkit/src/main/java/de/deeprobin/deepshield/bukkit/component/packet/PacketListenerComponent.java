package de.deeprobin.deepshield.bukkit.component.packet;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketEvent;
import de.deeprobin.deepshield.bukkit.Result;

public interface PacketListenerComponent {
    PacketType[] listenTo();
    Result onPacketReceiving(PacketEvent event);
}
