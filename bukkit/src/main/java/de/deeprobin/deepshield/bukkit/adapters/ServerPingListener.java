package de.deeprobin.deepshield.bukkit.adapters;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;

import java.util.Objects;

public class ServerPingListener extends PacketAdapter {

    private final DeepShieldPlugin plugin;

    public ServerPingListener(final DeepShieldPlugin plugin) {
        super(plugin, PacketType.Status.Server.SERVER_INFO);
        this.plugin = plugin;
        this.plugin.getLogger().info("Initialized server ping protocol listener.");
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        //WrappedServerPing ping = event.getPacket().getServerPings().read(0);

        event.getPacket().getServerPings().modify(0, ping -> {
            Objects.requireNonNull(ping).setVersionName(String.format("§9DeepShield protected %s", ping.getVersionName()));
            return ping;
        });
    }
}
