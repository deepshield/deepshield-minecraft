package de.deeprobin.deepshield.bukkit.manager;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.Result;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;
import de.deeprobin.deepshield.bukkit.component.item.ItemComponent;
import de.deeprobin.deepshield.bukkit.component.item.book.BookMetaComponent;
import de.deeprobin.deepshield.bukkit.component.packet.AdvancedPacketListenerComponent;
import de.deeprobin.deepshield.bukkit.component.packet.PacketListenerComponent;
import lombok.Getter;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;

public final class ComponentManager {

    private final DeepShieldPlugin plugin;
    private final ProtocolManager protocolManager;

    public ComponentManager(DeepShieldPlugin plugin){
        this.plugin = plugin;
        this.plugin.getLogger().info("Initialized ComponentManager.");
        this.protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Getter
    private List<Component> components = new ArrayList<>();

    public void registerComponent(Component component){
        if(component.getClass().isAnnotationPresent(ComponentInfo.class)) {
            ComponentInfo annotation = component.getClass().getAnnotation(ComponentInfo.class);

            this.plugin.getLogger().info(String.format("Registered component %s.", annotation.name().toUpperCase()));
            components.add(component);
            if(component instanceof Listener){
                this.plugin.getServer().getPluginManager().registerEvents((Listener) component, this.plugin);
                this.plugin.getLogger().info(String.format("Registered component listener of component %s.", annotation.name().toUpperCase()));
            }

            if(component instanceof PacketListenerComponent){
                PacketListenerComponent pComponent = (PacketListenerComponent) component;
                this.protocolManager.addPacketListener(new PacketAdapter(this.plugin, pComponent.listenTo()) {
                    @Override
                    public void onPacketReceiving(PacketEvent event) {
                        super.onPacketReceiving(event);
                        Result result = pComponent.onPacketReceiving(event);
                        if(result.isExploit()) {
                            // TODO: config option - message enabled
                            // TODO: use ComponentBuilder (not from md-5 - write own or use something from mojang)
                            // TODO: add event
                            // TODO: send message to admin - if enabled in config
                            event.getPlayer().sendMessage("§cThat is not allowed!");
                            event.setCancelled(true);
                        }
                    }

                    @Override
                    public void onPacketSending(PacketEvent event){
                        super.onPacketSending(event);
                        if(pComponent instanceof AdvancedPacketListenerComponent) {
                            Result result = ((AdvancedPacketListenerComponent)pComponent).onPacketSending(event);
                            if(result.isExploit()) {
                                // TODO: config option - message enabled
                                // TODO: use ComponentBuilder (not from md-5 - write own or use something from mojang)
                                // TODO: add event
                                // TODO: send message to admin - if enabled in config
                                event.getPlayer().sendMessage("§cThat is not allowed!");
                                event.setCancelled(true);
                            }
                        }
                    }
                });
            }
        } else {
            this.plugin.getLogger().warning(String.format("Cannot register component %s because it has no %s annotation", component.getClass().getName(), ComponentInfo.class.getName()));
        }
    }

    public Result[] getItemVulnerabilities(ItemStack itemStack) {
        List<Result> results = new ArrayList<>();
        for(Component component : components){
            if(component instanceof ItemComponent){
                ItemComponent itemComponent = (ItemComponent) component;
                Result result = itemComponent.isExploit(itemStack);
                if(result.isExploit()) {
                    results.add(result);
                }
            }
        }
        return results.toArray(new Result[0]);
    }

    public Result[] checkItem(ItemStack itemStack) {
        List<Result> results = new ArrayList<>();
        for(Component component : components){
            if(component instanceof ItemComponent){
                ItemComponent itemComponent = (ItemComponent) component;
                results.add(itemComponent.isExploit(itemStack));
            }
        }
        return results.toArray(new Result[0]);
    }

    public Result[] checkBook(BookMeta bookMeta) {
        List<Result> results = new ArrayList<>();
        for(Component component : components){
            if(component instanceof BookMetaComponent){
                BookMetaComponent bookComponent = (BookMetaComponent) component;
                results.add(bookComponent.checkBookMeta(bookMeta));
            }
        }
        return results.toArray(new Result[0]);
    }

}
