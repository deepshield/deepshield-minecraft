package de.deeprobin.deepshield.bukkit.worker;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.PluginResult;
import de.deeprobin.deepshield.core.Significance;
import de.deeprobin.deepshield.bukkit.component.Component;
import de.deeprobin.deepshield.bukkit.component.plugin.PluginComponent;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.net.URISyntaxException;

public class PluginChecker implements Runnable {

    private DeepShieldPlugin plugin;

    public PluginChecker(DeepShieldPlugin plugin){
        this.plugin = plugin;
    }

    public void run(){
        this.plugin.getLogger().info("Checking plugins: STARTED");
        for(Plugin cPlugin : plugin.getServer().getPluginManager().getPlugins()) {
            try {
                File file = new File(cPlugin.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
                for (Component component : this.plugin.getComponentManager().getComponents()) {
                    if (component instanceof PluginComponent) {
                        PluginResult result = ((PluginComponent) component).checkPlugin(file);
                        if(result.isVulnerable()){
                            if(result.getSignificance().equals(Significance.CRITICAL)) {
                                this.plugin.getServer().getPluginManager().disablePlugin(cPlugin);
                                if(file.renameTo(new File(file.getName() + ".qua"))){
                                    this.plugin.getLogger().severe(String.format("Moved vulnerable plugin %s (%s) [%s] to quarantine", cPlugin.getName(), file.getName(), result.getAdditionalInformation()));
                                    this.plugin.reportToStaff(String.format("Found critical vulnerability in plugin %s (%s) [%s] -> Moved to quarantine", cPlugin.getName(), file.getName(), result.getAdditionalInformation()));
                                }
                            } else {
                                this.plugin.reportToStaff(String.format("Found vulnerability in plugin %s (%s) [%s]", cPlugin.getName(), file.getName(), result.getAdditionalInformation()));
                            }

                        }
                    }
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }
        this.plugin.getLogger().info("Checking plugins: DONE");
    }
}
