package de.deeprobin.deepshield.bukkit.component.mod;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.SignChangeEvent;

@ComponentInfo(name = "sign.line-overflow")
public class SignComponent extends ClientModComponent {
    public SignComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @EventHandler
    public void handleSignChange(final SignChangeEvent event){
        for(String line : event.getLines()){
            if(line.length() >= 46){
                detect(event.getPlayer(), String.format("Sign Overflow(%d)", line.length()));
                event.setCancelled(true);
                return;
            }
        }
    }
}
