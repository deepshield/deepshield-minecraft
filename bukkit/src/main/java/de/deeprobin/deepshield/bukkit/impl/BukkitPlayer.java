package de.deeprobin.deepshield.bukkit.impl;

import de.deeprobin.deepshield.anticheat.ACPlayer;
import de.deeprobin.deepshield.anticheat.GameMode;
import de.deeprobin.deepshield.anticheat.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

public class BukkitPlayer implements ACPlayer {

    private final Player player;

    public BukkitPlayer(Player player) {
        this.player = player;
    }

    @Override
    public Location getLocation() {
        return ConvUtil.toAC(this.player.getLocation());
    }

    @Override
    public Location getEyeLocation() {
        return ConvUtil.toAC(this.player.getEyeLocation());
    }

    @Override
    public boolean isBlocking() {
        return this.player.isBlocking();
    }

    @Override
    public boolean isSleeping() {
        return this.player.isSleeping();
    }

    @Override
    public boolean isDead() {
        return this.player.isDead();
    }

    @Override
    public GameMode getGameMode() {
        return ConvUtil.toAC(this.player.getGameMode());
    }

    @Override
    public UUID getUniqueId() {
        return this.player.getUniqueId();
    }

    @Override
    public String getName() {
        return this.player.getName();
    }
}
