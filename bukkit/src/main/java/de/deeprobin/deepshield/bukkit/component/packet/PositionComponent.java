package de.deeprobin.deepshield.bukkit.component.packet;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketEvent;
import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.Result;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;

import java.util.HashMap;
import java.util.Map;

@ComponentInfo(name = "position")
public class PositionComponent extends Component implements PacketListenerComponent {
    public PositionComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @Override
    public PacketType[] listenTo() {
        return new PacketType[] {
                PacketType.Play.Client.POSITION,
                PacketType.Play.Client.POSITION_LOOK
        };
    }

    private Map<String, Double> lastY = new HashMap<>();
    private Map<String, Double> lastZ = new HashMap<>();

    @Override
    public Result onPacketReceiving(PacketEvent event) {
        double x = event.getPacket().getDoubles().read(0);
        double y = event.getPacket().getDoubles().read(1);
        double z = event.getPacket().getDoubles().read(2);
        if (!this.lastY.containsKey(event.getPlayer().getName()))
            this.lastY.put(event.getPlayer().getName(), y);
        if (!this.lastZ.containsKey(event.getPlayer().getName()))
            this.lastZ.put(event.getPlayer().getName(), z);
        if (y - this.lastY.get(event.getPlayer().getName()) == 9.0D) {
            event.setCancelled(true);
            this.lastY.remove(event.getPlayer().getName());
        } else if (z - this.lastZ.get(event.getPlayer().getName()) == 9.0D) {
            event.setCancelled(true);
            this.lastZ.remove(event.getPlayer().getName());
        }
        if (x == Double.MIN_VALUE || x == Double.MAX_VALUE || z == Double.MIN_VALUE || z == Double.MAX_VALUE) {
            event.setCancelled(true);
            return new Result(true, "Position payload");
        }
        return new Result(false);
    }
}
