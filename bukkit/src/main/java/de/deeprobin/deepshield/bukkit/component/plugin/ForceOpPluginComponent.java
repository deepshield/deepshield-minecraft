package de.deeprobin.deepshield.bukkit.component.plugin;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.PluginResult;
import de.deeprobin.deepshield.core.Significance;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;
import de.deeprobin.deepshield.core.util.AsmUtil;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@ComponentInfo(name = "plugin.forceop")
public class ForceOpPluginComponent extends Component implements PluginComponent {
    public ForceOpPluginComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @Override
    public PluginResult checkPlugin(File file) {
        try {
            final Map<String, ClassNode> classes = AsmUtil.loadClasses(file);
            for(ClassNode classNode : classes.values()){
                for (MethodNode method : classNode.methods) {
                    for (AbstractInsnNode instruction : method.instructions.toArray()) {
                        if(instruction instanceof MethodInsnNode){
                            MethodInsnNode methodInsnNode = (MethodInsnNode) instruction;
                            if(methodInsnNode.owner.equals("org/bukkit/entity/Player") || methodInsnNode.owner.equals("org/bukkit/entity/OfflinePlayer") || methodInsnNode.owner.equals("org/bukkit/entity/CommandSender")){
                                if(methodInsnNode.name.equals("setOp") && methodInsnNode.desc.equals("(Z)V")) {
                                    // It is definitly a force op plugin
                                    return new PluginResult(Significance.HIGH, "Force-OP");
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new PluginResult(false);
    }

}
