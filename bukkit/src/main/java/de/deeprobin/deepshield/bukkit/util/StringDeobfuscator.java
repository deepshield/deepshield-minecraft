package de.deeprobin.deepshield.bukkit.util;

public class StringDeobfuscator {
    // BlackSpigot Obfuscation
    public static String XORV1(String str) {
        try {
            char[] messageChars = str.toCharArray();
            char[] newMessage = new char[messageChars.length];
            char[] XORKEY = new char[]{'\u4832', '\u2385', '\u2386', '\u9813', '\u9125', '\u4582', '\u0913', '\u3422',
                    '\u0853', '\u0724'};
            char[] XORKEY2 = new char[]{'\u4820', '\u8403', '\u8753', '\u3802', '\u3840', '\u3894', '\u8739',
                    '\u1038', '\u8304', '\u3333'};
            int j = 0;
            while (j < messageChars.length) {
                newMessage[j] = (char) (messageChars[j] ^ XORKEY2[j % XORKEY2.length]);
                ++j;
            }
            char[] decryptedmsg = new char[newMessage.length];
            int j2 = 0;
            while (j2 < messageChars.length) {
                decryptedmsg[j2] = (char) (newMessage[j2] ^ XORKEY[j2 % XORKEY.length]);
                ++j2;
            }
            return new String(decryptedmsg);
        } catch (Exception ignore) {
            return str;
        }
    }

    public static String XORV2(String str) {
        try {
            char[] arrc = new char[]{'\u4831', '\u2384', '\u2385', '\u9812', '\u9123', '\u4581', '\u0912', '\u3421',
                    '\u0852', '\u0723'};
            char[] arrc2 = str.toCharArray();
            char[] arrc3 = new char[arrc2.length];
            for (int i = 0; i < arrc2.length; ++i) {
                arrc3[i] = (char) (arrc2[i] ^ arrc[i % arrc.length]);
            }
            return new String(arrc3);
        } catch (Exception exception) {
            return str;
        }
    }

    public static String XORV3(String string) {
        try {
            char[] arrc = new char[]{'\u4831', '\u2384', '\u2385', '\u9812', '\u9123', '\u4581', '\u0912', '\u3421',
                    '\u0852', '\u0723'};
            char[] arrc2 = string.toCharArray();
            char[] arrc3 = new char[arrc2.length];
            for (int i = 0; i < arrc2.length; ++i) {
                arrc3[i] = (char) (arrc2[i] ^ arrc[i % arrc.length]);
            }
            return new String(arrc3);
        } catch (Exception exception) {
            return string;
        }
    }

    public static String decryptionArray(String msg) {
        try {
            char[] array = {'\u4831', '\u2384', '\u2385', '\u9812', '\u9123', '\u4581', '\u0912', '\u3421', '\u0852',
                    '\u0723'};
            char[] charArray = msg.toCharArray();
            char[] array2 = new char[charArray.length];
            for (int i = 0; i < charArray.length; ++i) {
                array2[i] = (char) (charArray[i] ^ array[i % array.length]);
            }
            return new String(array2);
        } catch (Exception ex) {
            return msg;
        }
    }
}
