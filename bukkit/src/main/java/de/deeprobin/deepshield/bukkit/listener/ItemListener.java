package de.deeprobin.deepshield.bukkit.listener;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.Result;
import org.bukkit.GameMode;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;

public final class ItemListener implements Listener {

    private final DeepShieldPlugin plugin;

    public ItemListener(final DeepShieldPlugin plugin){
        this.plugin = plugin;
        this.plugin.getLogger().info("Initialized item listener.");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCreativeInventory(InventoryCreativeEvent event) {
        if (event.isCancelled()) {
            return;
        }
        HumanEntity whoClicked = event.getWhoClicked();
        if (whoClicked instanceof Player && whoClicked.getGameMode() == GameMode.CREATIVE) {
            Player player = (Player) whoClicked;
            for(Result result : this.plugin.getComponentManager().getItemVulnerabilities(event.getCurrentItem())){
                if(result.isExploit()){
                    if(!player.hasPermission("deepshield.bypass")) {

                        // TODO: config option - message enabled
                        // TODO: use ComponentBuilder (not from md-5 - write own or use something from mojang)
                        // TODO: add event
                        // TODO: send message to admin - if enabled in config
                        this.plugin.reportToStaff(String.format("%s tried to get a vulnerable item [%s]", player.getName(), result.getAdditionalInformation()));
                        player.sendMessage("§cThat is not allowed!");
                        event.setCancelled(true);
                        return;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent event){
        Player player = event.getPlayer();
        for(Result result : this.plugin.getComponentManager().getItemVulnerabilities(event.getItemDrop().getItemStack())){
            if(!player.hasPermission("deepshield.bypass")) {
                // TODO: config option - message enabled
                // TODO: use ComponentBuilder (not from md-5 - write own or use something from mojang)
                // TODO: add event
                // TODO: send message to admin - if enabled in config
                this.plugin.reportToStaff(String.format("%s tried to drop a vulnerable item [%s]", player.getName(), result.getAdditionalInformation()));
                player.sendMessage("§cThat is not allowed!");
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler
    public void onBookChange(PlayerEditBookEvent event) {
        Player player = event.getPlayer();
        for (Result result : this.plugin.getComponentManager().checkBook(event.getNewBookMeta())) {
            if (result.isExploit()) {
                // TODO: config option - message enabled
                // TODO: use ComponentBuilder (not from md-5 - write own or use something from mojang)
                // TODO: add event
                // TODO: send message to admin - if enabled in config
                this.plugin.reportToStaff(String.format("%s tried to get a nbt modified book [%s]", player.getName(), result.getAdditionalInformation()));
                player.sendMessage("§cThat is not allowed!");
                event.setCancelled(true);
                return;
            }
        }
    }

}
