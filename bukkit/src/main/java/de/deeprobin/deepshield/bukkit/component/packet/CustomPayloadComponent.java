package de.deeprobin.deepshield.bukkit.component.packet;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.Result;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@ComponentInfo(name = "custom_payload")
public class CustomPayloadComponent extends Component implements PacketListenerComponent {

    public CustomPayloadComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    private Map<UUID, Integer> channels = new HashMap<>();
    private Map<UUID, Integer> interactions = new HashMap<>();
    private Map<UUID, Integer> otherInteractions = new HashMap<>();

    @Override
    public PacketType[] listenTo() {
        return new PacketType[] { PacketType.Play.Client.CUSTOM_PAYLOAD };
    }

    @Override
    public Result onPacketReceiving(PacketEvent event) {
        if (!event.isCancelled()) {
            PacketContainer packetContainer = event.getPacket();
            String tag = "";
            Player player = event.getPlayer();
            List<Object> values = packetContainer.getModifier().getValues();

            if (!values.isEmpty()) {
                tag = String.valueOf(values.get(0));
            }
            if (tag == null) {
                event.setCancelled(true);
                return new Result(true);
            } else if (tag.equals("MC|BSign") || tag.equals("MC|BEdit") || tag.equals("MC|BOpen")) {

                int interactions = 0;
                if (this.interactions.containsKey(player.getUniqueId())) {
                    interactions = this.interactions.get(player.getUniqueId());
                }
                interactions++;
                this.interactions.put(player.getUniqueId(), interactions);

                if (interactions == 6) {
                    return new Result(true);
                } else if (interactions > 6) {
                    event.setCancelled(true);
                }
            } else if (tag.equals("REGISTER")) {
                int channels = 0;
                if (this.interactions.containsKey(player.getUniqueId())) {
                    channels = this.channels.get(player.getUniqueId());
                }
                channels++;
                this.channels.put(player.getUniqueId(), channels);

                if (channels == 128) {
                    event.setCancelled(true);
                    return new Result(true);
                } else if (channels > 128) {
                    event.setCancelled(true);
                }
            } else {
                int otherInteractions = 0;
                if (this.otherInteractions.containsKey(player.getUniqueId())) {
                    otherInteractions = this.otherInteractions.get(player.getUniqueId());
                }
                otherInteractions++;
                this.otherInteractions.put(player.getUniqueId(), otherInteractions);

                if (otherInteractions == 400) {
                    event.setCancelled(true);
                    return new Result(true);
                } else if (otherInteractions > 400) {
                    event.setCancelled(true);
                }
            }

        }
        return new Result(false);
    }
}
