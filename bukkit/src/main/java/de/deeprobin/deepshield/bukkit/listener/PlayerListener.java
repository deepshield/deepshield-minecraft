package de.deeprobin.deepshield.bukkit.listener;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitTask;

import java.util.function.Consumer;

public final class PlayerListener implements Listener {

    private final DeepShieldPlugin plugin;

    public PlayerListener(final DeepShieldPlugin plugin) {
        this.plugin = plugin;
        this.plugin.getLogger().info("Initialized player listener.");
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        /*PacketContainer packet = new PacketContainer(PacketType.Play.Server.CUSTOM_PAYLOAD);
        packet.getStrings().write(0, "minecraft:brand"); packet.getByteArrays().write(0, "DeepShield protected server".getBytes());
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(event.getPlayer(), packet);
        } catch (InvocationTargetException e) {
            this.plugin.getLogger().warning(String.format("A exception(%s) occurred [PLEASE CONTACT THE DEEPSHIELD DEVELOPERS]: %s", e.getClass().getName(), e.getMessage()));
        }*/

        Player player = event.getPlayer();
        this.plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Consumer<BukkitTask>() {

            private boolean x = false;

            @Override
            public void accept(BukkitTask task) {
                if (!player.isOnline()) {
                    task.cancel();
                    return;
                }
                x = !x;
                if(x) {
                    player.sendPluginMessage(plugin, "minecraft:brand", "§9DeepShield protected server".getBytes());
                } else {
                    player.sendPluginMessage(plugin, "minecraft:brand", "§bDeepShield protected server".getBytes());
                }
            }
        }, 0, 20);
    }

}
