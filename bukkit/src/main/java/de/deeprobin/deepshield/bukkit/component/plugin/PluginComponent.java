package de.deeprobin.deepshield.bukkit.component.plugin;


import de.deeprobin.deepshield.bukkit.PluginResult;

import java.io.File;

public interface PluginComponent {
    public PluginResult checkPlugin(File file);
}
