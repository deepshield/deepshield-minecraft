package de.deeprobin.deepshield.bukkit.component.item.book;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.Result;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;
import org.bukkit.inventory.meta.BookMeta;

@ComponentInfo(name = "book.page_size")
public class BookPageSizeComponent extends Component implements BookMetaComponent {
    public BookPageSizeComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @Override
    public Result checkBookMeta(BookMeta meta) {
        if(meta.getPageCount() > 100){
            new Result(true, String.format("Book Page Length(%d)", meta.getPageCount()));
        }
        return new Result(false);
    }
}
