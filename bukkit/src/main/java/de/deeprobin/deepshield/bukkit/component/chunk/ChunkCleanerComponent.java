package de.deeprobin.deepshield.bukkit.component.chunk;

import org.bukkit.Chunk;

public interface ChunkCleanerComponent {

    public void cleanChunk(Chunk chunk);

}
