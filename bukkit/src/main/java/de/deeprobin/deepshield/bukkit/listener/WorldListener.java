package de.deeprobin.deepshield.bukkit.listener;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.component.Component;
import de.deeprobin.deepshield.bukkit.component.chunk.ChunkCleanerComponent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

public final class WorldListener implements Listener {

    private final DeepShieldPlugin plugin;

    public WorldListener(final DeepShieldPlugin plugin) {
        this.plugin = plugin;
        this.plugin.getLogger().info("Initialized world listener.");
    }

    @EventHandler
    public void handleChunkLoad(ChunkLoadEvent event){
        if(!event.isNewChunk()) {
            for (Component component : this.plugin.getComponentManager().getComponents()) {
                if(component instanceof ChunkCleanerComponent){
                    ChunkCleanerComponent cleaner = (ChunkCleanerComponent) component;
                    cleaner.cleanChunk(event.getChunk());
                }
            }
        }
    }

}
