package de.deeprobin.deepshield.bukkit.component.packet;

import com.comphenix.protocol.events.PacketEvent;
import de.deeprobin.deepshield.bukkit.Result;

public interface AdvancedPacketListenerComponent extends PacketListenerComponent {
    Result onPacketSending(PacketEvent event);
}
