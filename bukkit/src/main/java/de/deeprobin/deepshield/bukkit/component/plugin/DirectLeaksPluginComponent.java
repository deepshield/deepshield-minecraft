package de.deeprobin.deepshield.bukkit.component.plugin;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.PluginResult;
import de.deeprobin.deepshield.core.Significance;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import de.deeprobin.deepshield.bukkit.component.Component;
import de.deeprobin.deepshield.bukkit.util.StringDeobfuscator;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.List;

@ComponentInfo(name = "plugin.direct-leaks")
public class DirectLeaksPluginComponent extends Component implements PluginComponent {
    public DirectLeaksPluginComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @Override
    public PluginResult checkPlugin(File file) {
        try {
            ZipFile zipFile = new ZipFile(file);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            InputStream inputStream = null;
            while(entries.hasMoreElements()){
                ZipEntry current = entries.nextElement();
                inputStream = zipFile.getInputStream(current);
                if(validClassPath(current.getName())){
                    ClassReader reader = new ClassReader(inputStream);
                    ClassNode classNode = new ClassNode();
                    reader.accept(classNode, 0);
                    List<MethodNode> nodes = classNode.methods;
                    for (MethodNode methodNode : nodes) {
                        for (AbstractInsnNode insnNode : methodNode.instructions.toArray()) {
                            if (insnNode instanceof LdcInsnNode && ((LdcInsnNode) insnNode).cst instanceof String) {
                                String string = ((LdcInsnNode) insnNode).cst.toString();
                                boolean xorv1 = containsBlacklistedWord(StringDeobfuscator.XORV1(string));
                                boolean xorv2 = containsBlacklistedWord(StringDeobfuscator.XORV2(string));
                                boolean xorv3 = containsBlacklistedWord(StringDeobfuscator.XORV3(string));
                                boolean xorv4 = containsBlacklistedWord(StringDeobfuscator.decryptionArray(string));
                                boolean sig = checkForDLSignature(classNode);
                                boolean bootstrap1 = bootstrap1(classNode);
                                boolean bootstrap2 = bootstrap2(classNode);
                                boolean host = false;
                                try {
                                    host = doesDLHostCheckExist(classNode);
                                } catch (Throwable ex) {
                                    ex.printStackTrace();
                                }
                                if (xorv1 || xorv2 || xorv3 || xorv4 || host || sig || bootstrap1 || bootstrap2) {

                                    return new PluginResult(Significance.MEDIUM, "DirectLeaks");
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean validClassPath(String classPath) {
        return classPath.endsWith(".class") && !classPath.contains("__MACOSX");
    }

    public boolean containsBlacklistedWord(String string) {
        String[] blacklistedWords = new String[]{"#directleaks", "Please contact DirectLeaks. 0x2",
                "http://api.directleaks.net/api/directleaks", "[DirectLeaks] Error Code: 0x1", "Anti-Releak",
                "DirectLeaks", "vmi209890.contaboserver.net", "167.86.75.51", "#DirectLeaks Anti-Releak",
                "DirectLeaks-API", "de.xbrowniecodez.dlapi.Main", "de.xbrowniecodez.dlapi.HostsCheck"};
        for (String blacklistedWord : blacklistedWords) {
            if (string.contains(blacklistedWord)) {
                return true;
            }
        }
        return false;
    }

    public static boolean doesDLHostCheckExist(ClassNode classNode) throws Throwable {
        Iterator<MethodNode> iterator = classNode.methods.iterator();
        while (iterator.hasNext()) {
            MethodNode attributeIterator = iterator.next();
            if (attributeIterator.name.equalsIgnoreCase("onEnable")) {
                InsnList attribute = attributeIterator.instructions;
                AbstractInsnNode insnNode = attribute.get(0);

                if (insnNode.getType() == 5 && insnNode.getOpcode() == 184
                        && ((MethodInsnNode) insnNode).desc.equals("()V")
                        && !((MethodInsnNode) insnNode).name.equalsIgnoreCase("loadConfig0")) {
                    return true;
                }
            } else if (attributeIterator.desc.equals("(Ljava/lang/String;)Ljava/lang/String;")
                    && attributeIterator.access == 4170) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkForDLSignature(ClassNode classNode) {
        if (classNode.superName.equals("org/bukkit/plugin/java/JavaPlugin")
                || classNode.superName.equals("net/md_5/bungee/api/plugin/Plugin")) {
            if (classNode.signature == null) {
                return false;
            }
            return classNode.signature.contains("directleaks");
        }
        return false;
    }

    public static boolean bootstrap1(ClassNode classNode) {
        Iterator<MethodNode> iterator = classNode.methods.iterator();
        while (iterator.hasNext()) {
            MethodNode methodNode = iterator.next();
            if (methodNode.desc.equalsIgnoreCase(
                    "(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/Class;Ljava/lang/String;I)Ljava/lang/invoke/CallSite;")) {
                return true;
            }
        }
        return false;
    }

    public static boolean bootstrap2(ClassNode classNode) {
        if (classNode.superName.equals("org/bukkit/plugin/java/JavaPlugin")
                || classNode.superName.equals("net/md_5/bungee/api/plugin/Plugin")) {
            for (FieldNode fieldNode : classNode.fields) {
                if (fieldNode.access == 9 && fieldNode.desc.equalsIgnoreCase("I") && fieldNode.name.length() == 36) {
                    return true;
                } else if (fieldNode.access == 9 && fieldNode.desc.equalsIgnoreCase("Ljava/lang/String;")
                        && fieldNode.name.length() == 36) {
                    return true;
                }
            }
        }
        return false;
    }
}
