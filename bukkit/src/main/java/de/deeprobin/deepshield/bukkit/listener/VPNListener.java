package de.deeprobin.deepshield.bukkit.listener;

import de.deeprobin.deepshield.core.util.VPNUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

public final class VPNListener implements Listener {

    @EventHandler
    public void handlePreLogin(AsyncPlayerPreLoginEvent event) {
        if(!event.getAddress().isAnyLocalAddress()) {
            if(VPNUtil.isVPN(event.getAddress().toString())){
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "§9DeepShield\n§6" + event.getAddress().toString() +"\n§cYour IP address is blocked for security reasons.\n§c§lDisable your proxy to enter the server");
            }
        }
    }

}
