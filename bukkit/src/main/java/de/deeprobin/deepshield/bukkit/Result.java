package de.deeprobin.deepshield.bukkit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class Result {

    public Result(boolean exploit){
        this.exploit = exploit;
    }

    public Result(boolean exploit, String additionalInformation){
        this(exploit);
        this.additionalInformation = additionalInformation;
    }

    private final boolean exploit;

    private String additionalInformation = "No additional information";

    public boolean isExploit() {
        return exploit;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }
}
