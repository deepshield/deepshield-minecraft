package de.deeprobin.deepshield.bukkit.component.mod;

import de.deeprobin.deepshield.bukkit.DeepShieldPlugin;
import de.deeprobin.deepshield.bukkit.annotation.ComponentInfo;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

@ComponentInfo(name = "mod.invmove")
public class InventoryMoveComponent extends ClientModComponent {
    public InventoryMoveComponent(DeepShieldPlugin plugin) {
        super(plugin);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        if(player.getOpenInventory() != null){

        }
    }
}
