package de.deeprobin.deepshield.bukkit.impl;

import de.deeprobin.deepshield.anticheat.GameMode;
import de.deeprobin.deepshield.anticheat.Location;

public class ConvUtil {
    public static Location toAC(org.bukkit.Location location){
        return new Location(location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
    }

    public static GameMode toAC(org.bukkit.GameMode mode){
        switch (mode){
            case CREATIVE:
                return GameMode.CREATIVE;
            case SURVIVAL:
                return GameMode.SURVIVAL;
            case ADVENTURE:
                return GameMode.ADVENTURE;
            case SPECTATOR:
                return GameMode.SPECTATOR;
            default:
                return null;
        }
    }
}
