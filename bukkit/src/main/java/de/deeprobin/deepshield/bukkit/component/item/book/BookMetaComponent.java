package de.deeprobin.deepshield.bukkit.component.item.book;


import de.deeprobin.deepshield.bukkit.Result;
import org.bukkit.inventory.meta.BookMeta;

public interface BookMetaComponent {

    Result checkBookMeta(BookMeta meta);

}
